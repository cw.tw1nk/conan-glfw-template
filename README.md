#Conan GLFW Template
___

Install https://cmake.org/download/

Install https://conan.io/downloads.html

###Clion

![img.png](img/img.png)
![img.png](img/img2.png)

###VS

install plugin
https://marketplace.visualstudio.com/items?itemName=conan-io.conan-vs-extension

...

![img.png](img/img3.png)
![img.png](img/img4.png)
###Terminal
```bash
mkdir build
cd build
conan install ..
```

######Windows

```bash
cmake .. -G "Visual Studio 15 2017"
cmake --build . --config Release
```

######Mac/Linux
```bash
cmake .. -G "Unix Makefiles" -DCMAKE_BUILD_TYPE=Release
cmake --build .
```
