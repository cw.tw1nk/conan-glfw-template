#include <iostream>
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <glm/glm.hpp>
#include <glm/gtc/random.hpp>
#include <complex>
#include <vector>
#include "FTUtils.h"
#include "GLUtils.h"

#define HEIGHT 480
#define WIDTH 640

void render(GLFWwindow *window, int width, int height);

int main(int, char **) {
    GLFWwindow *window;

    /* Initialize glfw lib */
    if (!glfwInit())
        return -1;

    /* Create a windowed mode window and its OpenGL context */
    window = glfwCreateWindow(WIDTH, HEIGHT, "Hello World", NULL, NULL);
    if (!window) {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    /* Initialize glad lib */
    if (!gladLoadGLLoader((GLADloadproc) glfwGetProcAddress)) {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }



//    FTText text("fonts/arial.ttf");
//    const std::vector<glm::vec2> &vector = calcPoints(vertex, WIDTH, HEIGHT, 100000);
    /* Loop until the user closes the window */
    while (!glfwWindowShouldClose(window)) {
        if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
            glfwSetWindowShouldClose(window, GL_TRUE);
        int width, height;
        glfwGetFramebufferSize(window, &width, &height);
//        text.render_text("Test", 0, 100, 50, 200);


        render(window, width, height);
        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();
    return 0;
}

Color color = rgb(0xFFDAB9);

void render(GLFWwindow *window, int width, int height) {
    glDisable(GL_DEPTH_TEST);
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();
    glOrtho(0, width, height, 0, 0, 1);
    glMatrixMode(GL_MODELVIEW);
    glViewport(0, 0, width, height);
    color.clearColor();
    glClear(GL_COLOR_BUFFER_BIT);
//    ...

}


