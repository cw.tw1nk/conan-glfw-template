//
// Created by twink on 25.01.2022.
//

#include "GLUtils.h"

Color rgb(unsigned int color) {
    Color temp = {static_cast<float>(((color & 0xFF0000) >> 16)) / 0xff,
                  static_cast<float>((color & 0x00FF00) >> 8) / 0xff, static_cast<float>((color & 0x0000FF)) / 0xff, 1};
    return temp;
}

Color rgba(unsigned int color) {
    Color temp = {static_cast<float>(((color & 0xFF000000) >> 24)) / 0xff,
                  static_cast<float>(((color & 0x00FF0000) >> 16)) / 0xff,
                  static_cast<float>((color & 0x0000FF00) >> 8) / 0xff,
                  static_cast<float>((color & 0x000000FF)) / 0xff};
    return temp;
}

void drawLine(int x, int y, int len, float angle, Color color, float w) {
    glPushMatrix();
    glTranslatef(x, y, 0);
    glRotatef(angle, 0, 0., 1.);
    glLineWidth(w);
    glBegin(GL_LINES);
    glColor4f(color.red, color.green, color.blue, color.alpha);
    {
        glVertex2f(0, 0);
        glVertex2f(len, 0);
    }
    glEnd();
    glPopMatrix();
}

void drawLine(int x1, int y1, int x2, int y2, Color color, float w) {
    glLineWidth(w);
    glBegin(GL_LINES);
    glColor4f(color.red, color.green, color.blue, color.alpha);
    {
        glVertex2f(x1, y1);
        glVertex2f(x2, y2);
    }
    glEnd();
}

void drawLine(Line line, Color color, float w) {
    drawLine(line.point1.x, line.point1.y, line.point2.x, line.point2.y, color, w);
}

void drawSquare(int x, int y, int w, int h, Color color) {
    glPushMatrix();
    glTranslatef(x, y, 0);
    glBegin(GL_QUADS);
    glColor4f(color.red, color.green, color.blue, color.alpha);
    {
        glVertex2f(0, 0);
        glVertex2f(w, 0);
        glVertex2f(w, h);
        glVertex2f(0, h);
    }
    glEnd();
    glPopMatrix();
}

Point center(Shape shape) {
    return { shape.point.x + shape.size.width/2, shape.point.y + shape.size.height/2 };
}

Point center(Line line) {
    return {(line.point1.x + line.point2.x)/2, (line.point1.y + line.point2.y)/2};
}

void drawPoint(int x, int y, float size, Color color) {
    glPointSize(size);
    glBegin(GL_POINTS);
    glColor4f(color.red, color.green, color.blue, color.alpha);
    {
        glVertex2i(x, y);
    }
    glEnd();
    glFlush();
}

void drawPoint(glm::vec2 vec, float size, Color color) {
    glPointSize(size);
    glBegin(GL_POINTS);
    glColor4f(color.red, color.green, color.blue, color.alpha);
    {
        glVertex2f(vec.x, vec.y);
    }
    glEnd();
    glFlush();
}

void drawPoint(std::vector<glm::vec2> v, float size, Color color) {
    glPointSize(size);
    glBegin(GL_POINTS);
    {
        glColor4f(color.red, color.green, color.blue, color.alpha);
        for (const auto &item : v) glVertex2f(item.x, item.y);
    }
    glEnd();
    glFlush();
}

void Color::clearColor() {
    glClearColor(this->red, this->green, this->blue, this->alpha);
}


