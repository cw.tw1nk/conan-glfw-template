//
// Created by twink on 25.01.2022.
//

#ifndef PROJ_GLUTILS_H
#define PROJ_GLUTILS_H
#include <glm/glm.hpp>
#include <vector>
#include "GLFW/glfw3.h"
#include <iostream>

struct Color {
    float red;
    float green;
    float blue;
    float alpha;

    void clearColor();
};

struct Point {
    int x;
    int y;
};

struct Size {
    int width;
    int height;
};

struct Shape {
    Point point;
    Size size;
};

struct Line {
    Point point1;
    Point point2;
};

namespace colors {
    const Color red = {1., 0., 0.};
    const Color green = {0., 1., 0.};
    const Color blue = {0., 0., 1.};
}

Color rgb(unsigned int color);

Color rgba(unsigned int color);

Point center(Shape shape);

Point center(Line line);

void drawPoint(int x, int y, float size = 1., Color color = colors::red);
void drawPoint(glm::vec2 vec, float size = 1., Color color = colors::red);
void drawPoint(std::vector<glm::vec2> v, float size = 1., Color color = colors::red);

void drawLine(int x, int y, int len, float angle = 0, Color color = colors::red, float w = 1.0);

void drawLine(int x1, int y1, int x2, int y2, Color color = colors::red, float w = 1.0);
void drawLine(Line line, Color color = colors::red, float w = 1.0);

void drawSquare(int x, int y, int w, int h, Color color);
#endif //PROJ_GLUTILS_H
