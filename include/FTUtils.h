//
// Created by twink on 02.02.2022.
//

#ifndef PROJ_FTUTILS_H
#define PROJ_FTUTILS_H

#include "glad/glad.h"
#include <exception>
#include <string>
#include <ft2build.h>
#include FT_FREETYPE_H

//todo
class FTText {
private:
    static bool flag_init;
    static FT_Library ft_lib;
    FT_Face face;
    FT_GlyphSlot g;
    static void init() {
        if (FT_Init_FreeType(&FTText::ft_lib)) {
            throw std::exception("Could not init freetype library");
        }
        flag_init = true;
    }
public:
    FTText(const char *ttf, int px_w = 0, int px_h = 48) {
        if (!flag_init) init();
        if (FT_New_Face(ft_lib, ttf, 0, &face)) {
            throw std::exception("Could not open font");
        }
        FT_Set_Pixel_Sizes(face, px_w, px_h);
        this->g = this->face->glyph;
    }

    void render_text(const char *text, float x, float y, float sx, float sy) {
        const char *p;

        for (p = text; *p; p++) {
            if (FT_Load_Char(face, *p, FT_LOAD_RENDER))
                continue;

            glTexImage2D(
                    GL_TEXTURE_2D,
                    0,
                    GL_RED,
                    g->bitmap.width,
                    g->bitmap.rows,
                    0,
                    GL_RED,
                    GL_UNSIGNED_BYTE,
                    g->bitmap.buffer
            );

            float x2 = x + g->bitmap_left * sx;
            float y2 = -y - g->bitmap_top * sy;
            float w = g->bitmap.width * sx;
            float h = g->bitmap.rows * sy;

            GLfloat box[4][4] = {
                    {x2,     -y2,     0, 0},
                    {x2 + w, -y2,     1, 0},
                    {x2,     -y2 - h, 0, 1},
                    {x2 + w, -y2 - h, 1, 1},
            };

            glBufferData(GL_ARRAY_BUFFER, sizeof box, box, GL_DYNAMIC_DRAW);
            glDrawArrays(GL_TRIANGLE_STRIP, 0, 4);

            x += (g->advance.x / 64) * sx;
            y += (g->advance.y / 64) * sy;
        }
    }

    static const FT_LibraryRec_ *getFtLib() {
        return ft_lib;
    }

    const FT_FaceRec_ *getFace() const {
        return face;
    }

    const FT_GlyphSlotRec_ *getG() const {
        return g;
    }
};

#endif //PROJ_FTUTILS_H
