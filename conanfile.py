from conans import ConanFile, CMake


class Proj(ConanFile):
    settings = "os", "compiler", "build_type", "arch"
    requires = "glfw/3.3.2", \
               "glew/2.1.0", \
               "glm/0.9.9.8", \
               "freetype/2.11.1", \
               "harfbuzz/3.2.0", \
               "glad/0.1.34"

    generators = "cmake"

    def build(self):
        cmake = CMake(self)
        cmake.configure()
        cmake.build()

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin")
        self.copy("*.dylib*", dst="bin", src="lib")
